SHELL = /bin/sh

#--------------------------------------------------------------------------
# Definitions
#--------------------------------------------------------------------------

MAKEPATH := $(abspath $(lastword $(MAKEFILE_LIST)))
PWD := $(dir $(MAKEPATH))

ASCIIDOCTOR_PDF_REQUIREMENT := asciidoctor-pdf

DOCKER_PACKAGE_NAME := docker
DOCKER_EXECUTABLE_NAME := $(DOCKER_PACKAGE_NAME)

MAKE_PDF_TYPE := docker

PDF_BUILD_DIR = ${PWD}build

SYSTEM_REQUIREMENTS :=

ifeq ($(MAKE_PRESENTATION_TYPE), native)
	SYSTEM_REQUIREMENTS += ${ASCIIDOCTOR_REQUIREMENT}
else
	SYSTEM_REQUIREMENTS += ${DOCKER_PACKAGE_NAME}
endif

#--------------------------------------------------------------------------
# Functions
#--------------------------------------------------------------------------

.PHONY: help check-requirements 
.PHONY: pdf

#: ## 
#-----------------------------: ## -----------------------------
#: ## GENERAL
#-----------------------------: ## -----------------------------

help: ## This help
	@awk 'BEGIN {FS = ":.*?## "} /^[\.%#a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

#: ## 
#-----------------------------: ## -----------------------------
#: ## Requirements
#-----------------------------: ## -----------------------------

$(SYSTEM_REQUIREMENTS):
	-@which "${@}" >/dev/null \
		&& echo "✓ ${@} is installed." \
		|| echo "✗ ${@} is not installed."

check-requirements: $(SYSTEM_REQUIREMENTS) ## Check for system requirements

#: ## 
#-----------------------------: ## -----------------------------
#: ## pdf
#-----------------------------: ## -----------------------------

pdf: ${PDF_BUILD_DIR}/index.pdf ## Build presentation from src/index.adoc

${PDF_BUILD_DIR}/index.pdf: src/*.adoc
#: ## 
#: ## Native asciidoctor will be used if installed. 
#: ## Otherwise docker will be used.
#: ## 
#: ## Override docker / native mode: 
#: ## 
#: ## example: make pdf MAKE_PDF_TYPE=native
#: ## example: make pdf MAKE_PDF_TYPE=docker 
ifeq ($(MAKE_PDF_TYPE), native)
	${ASCIIDOCTOR_PDF_REQUIREMENT}  \
		-r asciidoctor-diagram \
		--source-dir ${PWD}src \
		--destination-dir ${PDF_BUILD_DIR} \
		${PWD}src/index.adoc
else
	${DOCKER_EXECUTABLE_NAME} run -t --rm \
		--user 1000:1000 \
		-v "${PWD}src":/documents/ \
		-v "${PDF_BUILD_DIR}":/build/ \
		asciidoctor/docker-asciidoctor \
		asciidoctor-pdf  \
		-r asciidoctor-diagram \
		--source-dir /documents \
		--destination-dir /build \
		/documents/index.adoc
endif

